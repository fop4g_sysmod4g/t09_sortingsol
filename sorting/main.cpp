#include <assert.h>
#include <string>

#include "SFML/Graphics.hpp"
#include "Game.h"



using namespace sf;
using namespace std;



/*
Sprite demo with trees scrolling
*/
int main()
{

	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "My first code");

	Game game;
	game.Init(window);

	Clock clock;
	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
			if (event.type == Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
					window.close(); 
			}
		} 

		float elapsed = clock.getElapsedTime().asSeconds();
		clock.restart();

		game.Update(window, elapsed);

		// Clear screen
		window.clear();


		game.Render(window);

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}

