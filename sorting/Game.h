#pragma once
#include <vector>

#include "SFML/Graphics.hpp"

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};


/*
a tree object
*/
struct Tree
{
	float z=0;		//faked 3D depth - done using parallax and scaling 
	sf::Sprite spr; //image and position

	//move left at a depth proprotional speed
	//reset right after scrolling off screen
	void Update(sf::RenderWindow& window, float dT);
};

/*
Creates a fake 3D tree scene with layered background
*/
struct Game
{
	sf::Texture texBgnd, texTrees, texSky, texEarth;
	std::vector<Tree> trees;	//lots of trees
	sf::Sprite sprBgnd;			//background is scrolled in the texture, a bit shakey
	float bgndOff = 0;			//value is used to scroll the background texture so it appears to move

	void Init(sf::RenderWindow& window);
	void Render(sf::RenderWindow& window);
	//dT=elapsed time
	void Update(sf::RenderWindow& window, float dT);
};

/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 640,384 };

	const char ESCAPE_KEY{ 27 };

	const float TREE_SPEED = 50.f;
	const int NUM_TREES = 200;
	//7 trees on an atlas
	const int MAX_TREEGFX = 7;
	const sf::IntRect TREE_SPR[MAX_TREEGFX] {
		{0,0,80,112},
		{80,0,80,112},
		{160,0,80,112},
		{240,0,80,112},
		{320,0,80,112},
		{400,0,80,112},
		{480,0,80,112}
	};
}

